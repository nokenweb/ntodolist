<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
class TodosController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

public function index(){
  return DB::table('todos')->get();
}

public function store(Request $request){
  DB::table('todos')->insert([
    'name' => $request->name
    ]);

    return $request->name;
}

public function update(Request $request, $id){
  return   DB::table('todos')->where('id',$id)->update([
      'isDone' => $request->isDone
      ]);
}






public function view($id){
return json_encode(DB::table('todos')
  ->where('id',$id)
  ->first());
}

public function delete($id){
  return DB::table('todos')->where('id', $id)->delete();
}


    //
}
