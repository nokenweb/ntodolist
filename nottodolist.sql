-- phpMyAdmin SQL Dump
-- version 4.7.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 14 Jan 2019 pada 22.45
-- Versi server: 10.1.29-MariaDB
-- Versi PHP: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nottodolist`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `todos`
--

CREATE TABLE `todos` (
  `id` int(5) NOT NULL,
  `name` mediumtext NOT NULL,
  `isDone` tinyint(1) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updateAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `todos`
--

INSERT INTO `todos` (`id`, `name`, `isDone`, `createdAt`, `updateAt`) VALUES
(1, 'kesatuxxx', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'dua dua dua', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'ini update ke empat', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'data ke empat', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'data ke empat', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'data ke lima', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'data ke enam', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'sa coba dia lagi', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `todos`
--
ALTER TABLE `todos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `todos`
--
ALTER TABLE `todos`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
